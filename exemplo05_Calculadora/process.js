const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra: 52 + "px",
        };//Fechamento return
    },//fechamento data
    methods:{
        getNumero(numero){ 
            this.ajusteTamanhoDisplay();

            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();     
            }
            else{
                if(this.operador == "="){
                    this.valorDisplay = "";
                    this.operador = null;
             
            }


                //this.valorDisplay = this.valorDisplay + numero;
                            //adiçao simplificada
                this.valorDisplay += numero.toString();            
            }
        },//Fechamento getNumero
        limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
            this.tamanhoLetra = 50 + "px";
        },//Fechamento Limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){             
                this.valorDisplay +=".";            
            }//Fechamento if            
        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case"+":
                            this.valorDisplay = ((this.numeroAtual + displayAtual).toFixed(4)*1).toString();
                            break;
                            
                        case"-":this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;

                        case "*":
                            this.valorDisplay = ((this.numeroAtual * displayAtual).toFixed(2)*1).toString();
                            break;
                        case"/":
                            this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                            if (displayAtual === 0 ){
                               this.valorDisplay = 'nao da';                                
                            } else {
                                this.valorDisplay = (this.numeroAtual / displayAtual)
                            }
                            break;     
                                                       
                            // if(this.valorDisplay.includes(".")){
                            //     const numDecimal = parseFloat(this.valorDisplay);
                            //     this.valorDisplay = (numDecimal.toFixed)).toString();
                            // }
                                                           

                    }//fim do switch
                    
                    this.numeroAnterior = this.
                    numeroAtual;
                    this.numeroatual = null;
                    
                    if(this.operador != "="){
                        this.operador = null;
                    }
                    
                    

                }//Fim do if
                else{
                    this.numeroAnterior = displayAtual;

                }//fim do else

            }//fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
            this.valorDisplay = "0";
            }
            this.ajusteTamanhoDisplay();
        },//fim operaçoes

        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 31){
                this.tamanhoLetra = 14 + "px";
            }
            else if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 20 + "px";
                return;
          
            }
            else if(this.valorDisplay.length >= 12){
                this.tamanhoLetra = 30 + "px";
                return;
            }
            else{
                this.tamanhoLetra = 50 + "px";
            }
        },

        
    },//Fechamento methods
}).mount("#app");//Fechamento app