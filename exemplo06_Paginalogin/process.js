const {createApp} = Vue;

createApp({
    data(){
        return{
            usuario: '',
            senha:'',
            erro: null,
            sucesso: null,
        }//fechamento return
    },//fechameno data

    methods:{
        login(){
            // alert("wwwtestando...");            
             //Simulando uma requisicao de login assincrona
            setTimeout(() => {
                if((this.usuario === "Lucas" && this.senha === "arroba") || 
                (this.usuario === "Luck" && this.senha === "aa")){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";

                        // alert("Login efetuado com sucesso!");
                }//fim do if
                else{
                    // alert("Usuario ou Senha incorretos!");
                    this.erro = "Usuario ou senha incorretos!";
                    this.sucesso = null;
              }
            }, 1000)
        },
    },//Fechamento methods       
}).mount("#app");//fechamento app