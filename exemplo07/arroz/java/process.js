const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra: 50+ "px",
             //login
             usuario: "",
             senha: "",
             erro:null,
             sucesso:null,
             //login
             //Arrays (vetores) para armazenamento des nomes de usuarios e senhas
             usuarios: ["admin", "Lucas", "Dani", "Vini"],
             senhas: ["aa", "bb", "cc", "dd"],
             userAdmin: false,
             mostrarEntrada: false,
             
             //variaveis para tratamento das informaçoes dos novos usuarios
             newUsername: "",
             newPassword: "",
             confirmPassword: "",

             mostrarLista: false,

        };//Fechamento return
    },//Fechamento data


    methods:{
        login(){
            // alert("Testando...");

            //Simuklando uma requisão de login assincrona
            setTimeout(() =>{
                if((this.usuario==="admin" && this.senha==="aa") || (this.usuario==="Dani" && this.senha==="bb") || (this.usuario==="Lucas" && this.senha==="cc") || (this.usuario==="Vini" && this.senha==="dd")){
                    // alert("Login efetuado com sucesso!");
                    this.erro = null;
                    this.sucesso = "login efetuado com sucesso!"
                    setTimeout(function(){
                        window.location.href='calculadora.html';
                    },400);
                }
                else{
                    // alert("Usuario ou senha  incorretos");
                    this.erro = "Usuario ou senha incorretos!";
                    this.sucesso = null;
                };//Fechameno if admin
                
                
               
                //Fechamento else admin
            },400);
        },//Fechamento Login

        login2() {
            this.mostrarEntrada = false;
        
            setTimeout(() => {
                this.mostrarEntrada = true;
                // Verificação de usuários e senhas cadastrados nos arrays
                const index = this.usuarios.indexOf(this.usuario);
                if (index !== -1 && this.senhas[index] === this.senha) {
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!"
        
                    // Registrando o usuário no localStorage para lembrete de acessos
                    localStorage.setItem("usuario", this.usuario);
                    localStorage.setItem("senha", this.senha);
        
                    // Verificando se o usuário é um administrador
                    if (
                        (this.usuario === "admin" && index === 0) ||
                        (this.usuario === "Dani" && index === 1) ||
                        (this.usuario === "Lucas" && index === 2) ||
                        (this.usuario === "Vini" && index === 3)
                    ) {
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!";
                    }
                } else {
                    this.sucesso = null;
                    this.erro = "Usuário e/ou senha incorretos!";
                }
            }, 1000);
        },
        
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                if (this.operador == "="){
                    this.valorDisplay = "";
                    this.operador=null;
                }
               //this.valorDisplay = this.valorDisplay + numero.toString();


                //adição simplificada
                this.valorDisplay += numero.toString();
            }
        },// Fechamento getNumero


        Limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
        },//Fechamento limpar


        decimal(){
                if(!this.valorDisplay.includes(".")){
                    this.valorDisplay += ".";
                }//Fechamento if
        },//Fechamento decimal


        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat
                (this.valorDisplay);
                if(this.operador!= null){
                    switch(this.operador){
                        case"+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                       
                        case"-":
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;
                             
                        case"*":
                            this.valorDisplay = (this.numeroAtual*displayAtual).toString();
                            break;
                        case"/":
                            this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                        if (displayAtual==0){this.valorDisplay = "não pode se dividir por 0";
                        }else{this.valorDisplay = (this.numeroAtual / displayAtual)}
                        break;
                    }//Fim do switch
                    this.numeroAnterior = this.numeroAtual
                    this.numeroAtual = null;
                    this.operador = null;
                     
                    if (this.operador != "="){
                        this.operador = null;
                    }


                    //acertar o número de casas decimais quando o resultado for decimal
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay + (numDecimal.toFixed(2)). toString();


                    }
                }//Fim do if
                else{
                    this.numeroAnterior = displayAtual;
                }//Fim do else
            }//Fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
            //this.valorDisplay = "0";
           
        },//fim operações
        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 30 + "px";
            }
            // else if()
        },

        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.erro = null;
                this.sucesso = "Carregando Página de Cadastro...";
                this.mostrarEntrada = true;

                //

                setTimeout(() => { }, 2000);

                setTimeout(() => {
                    window.location.href = "cadastro.html";
                }, 1000);
            }
            else{
               this.sucesso = null;
               setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.erro = "sem privilegios ADMIN!!!";
               }, 1000);
            }
        },

        adicionarUsuario(){
            this.mostrarEntrada = false;
            this.usuario = localStorage.getItem("usuario");
            this.senha = localStorage.getItem("senha");

            setTimeout(() => {
                this.mostrarEntrada = true;

                if(this.usuario === "admin"){
                    /*verificando de o novo usuario é diferente de vazio e se ele ja nao esta cadastrado no sistema*/
                    if(!this.usuarios.includes(this.newUsername) && this.newUsername !== "" && !this.newUsername.includes(" ")){
                        //Validaçao da senha digitada para o cadrasto no array
                        if(this.newPassword !== "" && !this.newPassword.includes(" ") && this.newPassword === this.confirmPassword){
                            //inserindo o novo usuario e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);

                            //atualizando o usuario recem cadastrado no LocalStorage
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                            this.erro = null;
                            this.sucesso = "Usuario cadastrado com sucesso";

                        }//Fechamento if password
                        else{
                            this.sucesso = null;
                            this.erro = "Por Favor,informe uma senha valida!!!";

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";
                        }
                    

                    }//Fechamento includes
                    else{
                        this.erro = "Usuario ja cadastrado ou Invalido! Por favor digite um usuario diferente!";
                        this.sucesso = null;

                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                    }//fehcamento else
                
                }
                else{
                    this.erro="Nao esta logado como admin";
                    this.sucesso = null;

                    this.newUsername = "";
                    this.newPassword = "";
                    this.confirmPassword = "";
                }
            }, 500);
        },

        listarUsuarios(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//fechamento if
            this.mostrarLista = !this.mostrarLista;

        },//fechamento ListaUsuarios

        excluirUsuario(usuario) {
            this.mostrarEntrada = false;
            if (usuario === "admin") {
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.erro = "O usuário ADMIN não pode ser excluído!";
                }, 400);
                return; // Força a saída deste bloco
            }
        
            if (confirm("Tem certeza que deseja excluir o usuário?")) {
                const index = this.usuarios.indexOf(usuario);
                if (index !== -1) {
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    // Atualiza a array usuarios no LocalStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
        
                    setTimeout(() => {
                        this.mostrarEntrada = true;
                        this.erro = null;
                        this.sucesso = "Usuário excluído com sucesso!";
                    }, 400);
                }
            }
        }
        
        

        
       
    },//Fechamento methods


}).mount("#app");//Fechamento app
